<div class="col  col-md-6 col-sm-12 col-xs-12">
<div class="pos-logo-container">
	<div class="pos-logo">
		<div class="pos-title"><h2><span>{l s='Our Brands' mod='poslogo'}</span></h2></div>
		<div class="row pos-content">
			<div class="logo-slider">
				{foreach from=$logos item=logo name=myLoop}
					{if $smarty.foreach.myLoop.index % 3 == 0 || $smarty.foreach.myLoop.first }
					<div>
					{/if}
						<div class="item-banklogo wow fadeInUp" data-wow-delay="{$smarty.foreach.myLoop.index}00ms">
							<a href ="{$logo.link}">
								<img class="replace-2x " src ="{$logo.image}" alt ="{l s='Logo' mod='poslogo'}" />
							</a>
						</div>
					{if $smarty.foreach.myLoop.iteration % 3 == 0 || $smarty.foreach.myLoop.last  }
					</div>
					{/if}	
				{/foreach}
			
			</div>
			<div class="boxprevnext">
				<a class="prev prevlogo"><i class="icon-angle-left"></i></a>
				<a class="next nextlogo"><i class="icon-angle-right"></i></a>
			</div> 
		</div>
			
	</div>		 
</div>
</div>
<script type="text/javascript"> 
		$(document).ready(function() {
			var owl = $(".logo-slider");
			owl.owlCarousel({
			items :3,
			slideSpeed: 1000,
			pagination : false,
			itemsDesktop : [1199,3],
			itemsDesktopSmall : [911,3],
			itemsTablet: [767,3],
			itemsMobile : [360,2]
			});
			$(".nextlogo").click(function(){
			owl.trigger('owl.next');
			})
			$(".prevlogo").click(function(){
			owl.trigger('owl.prev');
			})  
		});
</script>