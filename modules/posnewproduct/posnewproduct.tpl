<!--
<div class="pos-new-product col-md-3 col-sm-12 col-xs-12">
	<div class="pos-title"><h2>{l s='New Products' mod='posnewproduct'}</h2></div>
	{if count($products)>1}
		<div class=" row pos-content">
			<div class="pos_newproducts ">			
				{foreach from=$products item=product name=myLoop}
					{if $smarty.foreach.myLoop.index % 2 == 0 || $smarty.foreach.myLoop.first }
					<div class="newproductslider-item ajax_block_product">
					{/if}	
						<div class="item-product wow fadeInUp" data-wow-delay="{$smarty.foreach.myLoop.index}00ms">
							<div class="products-inner">
								<a href="{$product.link|escape:'html'}" title="{$product.name|escape:html:'UTF-8'}" class="product_image"><img class="replace-2x img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html'}" height="{$homeSize.height}" width="{$homeSize.width}" alt="{$product.name|escape:html:'UTF-8'}" />
								
								</a>
								{if isset($product.new) && $product.new == 1}
									<a class="new-box" href="{$product.link|escape:'html':'UTF-8'}">
										<span class="new-label">{l s='New' mod='posnewproduct'}</span>
									</a>
								{/if}
								{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
									<a class="sale-box" href="{$product.link|escape:'html':'UTF-8'}">
										<span class="sale-label">{l s='Sale!' mod='posnewproduct'}</span>
									</a>
								{/if}
								{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
									{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
										{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
											{if $product.specific_prices.reduction_type == 'percentage'}
												<span class="price-percent-reduction"><span>-{$product.specific_prices.reduction * 100}%</span></span>
											{/if}
										{/if}										
									{/if}						
								{/if}
								<div class="actions">
															
									<div class="actions-inner">
									
										<ul class="add-to-links">
											<li class="cart">
												{if ($product.id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $product.available_for_order && !isset($restricted_country_mode) && $product.minimal_quantity <= 1 && $product.customizable != 2 && !$PS_CATALOG_MODE}
												{if ($product.allow_oosp || $product.quantity > 0)}
												{if isset($static_token)}
													<a class="button ajax_add_to_cart_button btn btn-default" href="{$link->getPageLink('cart',false, NULL, "add=1&amp;id_product={$product.id_product|intval}&amp;token={$static_token}", false)|escape:'html':'UTF-8'}" rel="nofollow"  title="{l s='Add to cart' mod='posnewproduct'}" data-id-product="{$product.id_product|intval}">
														<span>{l s='Add to cart' mod='posnewproduct'}</span>
														
													</a>
												{else}
												<a class="button ajax_add_to_cart_button btn btn-default" href="{$link->getPageLink('cart',false, NULL, 'add=1&amp;id_product={$product.id_product|intval}', false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart' mod='posnewproduct'}" data-id-product="{$product.id_product|intval}">
													<span>{l s='Add to cart' mod='posnewproduct'}</span>
												</a>
												   {/if}      
												{else}
												<span class="button ajax_add_to_cart_button btn btn-default disabled" >
													<span>{l s='Add to cart' mod='posnewproduct'}</span>
												</span>
												{/if}
												{/if}
											</li>

											<li>
												<a class="addToWishlist wishlistProd_{$product.id_product|intval}" href="#" data-wishlist="{$product.id_product|intval}" title="{l s='Add to Wishlist' mod='posnewproduct'}" onclick="WishlistCart('wishlist_block_list', 'add', '{$product.id_product|intval}', false, 1); return false;">
													<span>{l s="Wishlist" mod='posnewproduct'}</span>
													
												</a>
											</li>
											<li>
											{if isset($comparator_max_item) && $comparator_max_item}
											  <a class="add_to_compare" href="{$product.link|escape:'html':'UTF-8'}" data-id-product="{$product.id_product}" title="{l s='Add to Compare' mod='posnewproduct'}">{l s='Compare' mod='posnewproduct'}
											
											  </a>
											 {/if}
											</li>
											<li>
												{if isset($quick_view) && $quick_view}
													<a class="quick-view" title="{l s='Quick view' mod='posnewproduct'}" href="{$product.link|escape:'html':'UTF-8'}">

													</a>
												{/if}
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="product-contents">
								<h5 class="product-name"><a href="{$product.link|escape:'html'}" title="{$product.name|truncate:50:'...'|escape:'htmlall':'UTF-8'}">{$product.name|truncate:35:'...'|escape:'htmlall':'UTF-8'}</a></h5>							
								<div class="product_desc"><a href="{$product.link|escape:'html'}" title="{l s='More' mod='posnewproduct'}">{$product.description_short|strip_tags|truncate:65:'...'}</a></div>
								{capture name='displayProductListReviews'}{hook h='displayProductListReviews' product=$product}{/capture}
								{if $smarty.capture.displayProductListReviews}
									<div class="hook-reviews">
									{hook h='displayProductListReviews' product=$product}
									</div>
								{/if}
								{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
								<div class="price-box">
								{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
									{hook h="displayProductPriceBlock" product=$product type='before_price'}
									<span class="price product-price">
										{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}
									</span>
									{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
										{hook h="displayProductPriceBlock" product=$product type="old_price"}
										<span class="old-price product-price">
											{displayWtPrice p=$product.price_without_reduction}
										</span>
										{hook h="displayProductPriceBlock" id_product=$product.id_product type="old_price"}
										
									{/if}
									{hook h="displayProductPriceBlock" product=$product type="price"}
									{hook h="displayProductPriceBlock" product=$product type="unit_price"}
									{hook h="displayProductPriceBlock" product=$product type='after_price'}
								{/if}
								</div>
								{/if}

							</div>
						</div>
					{if $smarty.foreach.myLoop.iteration % 2 == 0 || $smarty.foreach.myLoop.last  }		
					</div>
					{/if}
				{/foreach}
				
			</div>
			<div class="boxprevnext">
				<a class="prev prevnewproduct"><i class="icon-angle-left"></i></a>
				<a class="next nextnewproduct"><i class="icon-angle-right"></i></a>
			</div>
		</div>
	{else}
	<p class="warning">{l s='No products for this new products.'}</p>
	{/if}
</div>

<script>


    $(document).ready(function() {
     
    var owl = $(".pos_newproducts");
     
    owl.owlCarousel({
	autoPlay : false,
	 pagination :false,
    items : 1,
			slideSpeed: 1000,
		itemsDesktop : [1199,1],
		itemsDesktopSmall : [991,3],
		itemsTablet: [767,2],
		itemsMobile : [480,1]
    });
	// Custom Navigation Events
		$(".nextnewproduct").click(function(){
		owl.trigger('owl.next');
		})
		$(".prevnewproduct").click(function(){
		owl.trigger('owl.prev');
		})     
    });
</script>
		 
-->