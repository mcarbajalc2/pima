<?php
/**
 * 2008-today Mediacom87
 *
 * NOTICE OF LICENSE
 *
 * Read in the module
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Mediacom87 <support@mediacom87.net>
 * @copyright 2008-today Mediacom87
 * @license   define in the module
 */

if (!defined('_TB_VERSION_')
    && !defined('_PS_VERSION_')) {
    exit;
}

include_once dirname(__FILE__) . '/class/mediacom87.php';

class MedCaptchaFree extends Module
{
    public $smarty;
    public $context;
    public $controller;
    private $errors = array();
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'medcaptchafree';
        $this->tab = 'front_office_features';
        $this->version = '1.2.0';
        $this->author = 'Mediacom87';
        $this->need_instance = 0;
        $this->module_key = ''; // 6bebd0c5695be29f1cb29301c68246a1
        $this->addons_id = ''; //6170
        $this->ps_versions_compliancy = array('min' => '1.5.0.0', 'max' => '1.7.99.99');

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Free module to add a captcha');
        $this->description = $this->l('Secure your contact form with a captcha');

        $this->mediacom87 = new MedCaptchaFreeClass($this);

        $this->tpl_path = _PS_ROOT_DIR_.'/modules/'.$this->name;
    }

    /**
     * install function .
     *
     * @access public
     * @return void
     */
    public function install()
    {
        if (!parent::install()
            || !$this->registerHook('Header')
            || !$this->registerHook('ActionCaptchaEmail')
            || !$this->registerHook('Footer')) {
            return false;
        }

        if (version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            if (!$this->registerHook('actionFrontControllerSetMedia')) {
                return false;
            }
        }
        return true;
    }

    /**
     * uninstall function.
     *
     * @access public
     * @return void
     */
    public function uninstall()
    {
        if (!parent::uninstall()
            || !Configuration::deleteByName($this->name)) {
            return false;
        }

        return true;
    }

    /**
     * getContent function.
     *
     * @access public
     * @param string $tab (default: 'AdminModules')
     * @return void
     */
    public function getContent($tab = 'AdminModules')
    {
        $output = '';

        $this->context->smarty->assign(array(
                'tpl_path' => _PS_ROOT_DIR_ . '/modules/' . $this->name,
                'img_path' => $this->_path . 'views/img/',
                'description' => $this->description,
                'author' => $this->author,
                'name' => $this->name,
                'version' => $this->version,
                'ps_version' => _PS_VERSION_,
                'iso_code' => $this->mediacom87->isoCode(),
                'iso_domain' => $this->mediacom87->isoCode(true),
                'languages' => Language::getLanguages(false),
                'id_active_lang' => (int) $this->context->language->id,
                'link' => $this->context->link,
                'countries' => Country::getCountries((int) $this->context->language->id),
            ));

        $this->context->controller->addJS(array(
                $this->_path.'libraries/js/riotcompiler.min.js',
                $this->_path.'libraries/js/pageloader.js',
                $this->_path.'views/js/back.js'
            ));

        $this->context->controller->addCSS($this->_path.'views/css/back.css');

        $output .= $this->display(__FILE__, 'views/templates/admin/admin.tpl');
        $output .= $this->display(__FILE__, 'libraries/prestui/ps-tags.tpl');

        return $output;
    }

    /**
     * hookHeader function.
     *
     * @access public
     * @param mixed $params
     * @return void
     */
    public function hookHeader()
    {
        if ($this->active
            && isset($this->context->controller->php_self)
            && $this->context->controller->php_self == 'contact'
            && version_compare(_PS_VERSION_, '1.7.0.0', '<')) {
            $this->context->controller->addCSS($this->_path.'/views/css/front.css', 'all');
            return '<script src="https://authedmine.com/lib/captcha.min.js" async defer></script>';
        }
    }

    /**
     * hookDisplayHeader function.
     *
     * @access public
     * @return void
     */
    public function hookDisplayHeader()
    {
        return $this->hookHeader();
    }

    /**
     * hookContactFormBottom function.
     *
     * @access public
     * @return void
     */
    public function hookFooter()
    {
        return $this->displayCaptcha(Tools::getValue('medEr'));
    }

    /**
     * displayCaptcha function.
     *
     * @access protected
     * @return void
     */
    protected function displayCaptcha($medEr = false)
    {
        if ($this->active
            && isset($this->context->controller->php_self)
            && $this->context->controller->php_self == 'contact') {
            if ($medEr) {
                $this->context->smarty->assign('medEr', $medEr);
            }
            if (version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                $this->context->smarty->assign('contactClass', 'contact-form');
                return $this->fetch('module:'.$this->name.'/views/templates/hook/'.$this->name.'.tpl');
            } else {
                if (version_compare(_PS_VERSION_, '1.6.0.0', '>=')) {
                    $this->context->smarty->assign('contactClass', 'contact-form-box');
                    $this->context->smarty->assign('PS16', 1);
                } else {
                    $this->context->smarty->assign('contactClass', 'std');
                    $this->context->smarty->assign('PS15', 1);
                }
                return $this->display(__FILE__, 'views/templates/hook/'.$this->name.'.tpl');
            }
        }
    }

    /**
     * hookActionFrontControllerSetMedia function.
     *
     * @access public
     * @param mixed $params
     * @return void
     */
    public function hookActionFrontControllerSetMedia($params)
    {
        // Only on product page
        if ('contact' === $this->context->controller->php_self) {
            $this->context->controller->registerStylesheet(
                'module-medcaptchafree-style',
                'modules/'.$this->name.'/views/css/front.css',
                [
                'media' => 'all',
                'priority' => 200,
                ]
            );
            $this->context->controller->registerJavascript(
                'remote-medcaptchafree-js',
                'https://authedmine.com/lib/captcha.min.js',
                [
                'server' => 'remote',
                'position' => 'head',
                'priority' => 200,
                ]
            );
            $this->context->controller->registerJavascript(
                'module-medcaptchafree-js',
                'modules/'.$this->name.'/views/js/front.js',
                [
                'priority' => 200,
                ]
            );
        }
    }

    /**
     * hookActionEmailAddBeforeContent function.
     *
     * @access public
     * @param mixed $params
     * @return void
     */
    public function hookActionCaptchaEmail($params)
    {
        if (version_compare(_PS_VERSION_, '1.7', '<')
            || $params['template'] == 'contact') {
            $url = 'https://api.coinhive.com/token/verify';
            $post_data = array(
                'secret' => "Vk9ejUDQlfzG0HDKzzngXNEvuGDmYFHK",
                'token' => Tools::getValue('coinhive-captcha-token'),
                'hashes' => 256
            );

            $post_context = stream_context_create(array(
                    'http' => array(
                        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                        'method'  => 'POST',
                        'content' => http_build_query($post_data)
                    )
                ));

            $response = Tools::jsonDecode(Tools::file_get_contents($url, false, $post_context));

            if ($response && $response->success) {
                return;
            } else {
                $url = $this->context->link->getPageLink('contact');
                Tools::redirect($url.'?medEr=1');
                exit;
            }
        }
    }
}
