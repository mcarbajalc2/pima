{*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to http://doc.prestashop.com/display/PS15/Overriding+default+behaviors
* #Overridingdefaultbehaviors-Overridingamodule%27sbehavior for more information.
*
* @author Mediacom87 <support@mediacom87.net>
* @copyright  Mediacom87
* @license    commercial license see tab in the module
*}

<div class="coinhive-captcha"
	data-hashes="256"
	data-key="CDiOs8VyHBLSuVi7U15MRyAxIQbh2sbu"
	data-whitelabel="true"
	data-disable-elements=".{$contactClass} [type=submit]"
>
	<em>{l s='Loading Captcha...' mod='medcaptchafree'}<br>
	{l s='If it doesn\'t load, please disable Adblock!' mod='medcaptchafree'}</em>
</div>
{if isset($medEr) && $medEr}
    <div id="medEr" class="col-xs-12 alert alert-danger error">{l s='You forgot to validate the verification!' mod='medcaptchafree'}</div>
{/if}
{if isset($PS16) && $PS16}
    <script type="text/javascript">
         $(document).ready(function() {
            $('.contact-form-box [type=submit]').before($('div.coinhive-captcha'));
            $('div.coinhive-captcha').show();
            $('.contact-form-box').prepend($('#medEr'));
            $('#medEr').show();
        });
    </script>
{else if isset($PS15) && $PS15}
    <script type="text/javascript">
         $(document).ready(function() {
            $('form.std [type=submit]').before($('div.coinhive-captcha'));
            $('div.coinhive-captcha').css('text-align', 'center');
            $('div.coinhive-captcha').show();
            $('form.std').prepend($('#medEr'));
            $('#medEr').show();
        });
    </script>
{/if}
