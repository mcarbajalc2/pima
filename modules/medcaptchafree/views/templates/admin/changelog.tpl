{*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to http://doc.prestashop.com/display/PS15/Overriding+default+behaviors
* #Overridingdefaultbehaviors-Overridingamodule%27sbehavior for more information.
*
* @author Mediacom87 <support@mediacom87.net>
* @copyright  Mediacom87
* @license    commercial license see tab in the module
*}

<div class="medChangelog">

    <ps-panel header="1.2.0 - {dateFormat date='2018-04-15' full=0}">

        <ul>
            <li>Code corrections</li>
        </ul>

    </ps-panel>

    <ps-panel header="1.1.0 - {dateFormat date='2018-03-07' full=0}">

        <ul>
            <li>Code corrections</li>
        </ul>

    </ps-panel>

    <ps-panel header="1.0.0 - {dateFormat date='2018-03-05' full=0}">

        <ul>
            <li>Initial commit</li>
        </ul>

    </ps-panel>

</div>
