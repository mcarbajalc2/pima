{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{strip}
<span class="heading-counter">
{if (isset($category) && $category->id == 1) OR (isset($nb_products) && $nb_products == 0)}
	{if $lang_iso == 'es'}
		{l s='No hay productos es esta categoría.'}
	{else}
		{l s='There are no products in this category.'}
	{/if}	
{else}
	{if isset($nb_products) && $nb_products == 1}
		{if $lang_iso == 'es'}
			{l s='Hay 1 producto.'}
		{else}
			{l s='There is 1 product.'}
		{/if}
	{elseif isset($nb_products)}
		{if $lang_iso == 'es'}
			{l s='Hay %d productos.' sprintf=$nb_products}
		{else}
			{l s='There are %d products.' sprintf=$nb_products}
		{/if}
	{/if}
{/if}
</span>
{/strip}
