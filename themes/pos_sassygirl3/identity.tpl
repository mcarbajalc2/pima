{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}
    <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
        {if $lang_iso == 'es'}
            {l s='Mi cuenta'}
        {else}
            {l s='My account'}
        {/if}
    </a>
    <span class="navigation-pipe">
        {$navigationPipe}
    </span>
    <span class="navigation_page">
        {if $lang_iso == 'es'}
            {l s='Tu información personal'}
        {else}
            {l s='Your personal information'}
        {/if}
    </span>
{/capture}
<div class="box">
    <h1 class="page-subheading">
        {if $lang_iso == 'es'}
            {l s='Tu información personal'}
        {else}
            {l s='Your personal information'}
        {/if}
    </h1>

    {include file="$tpl_dir./errors.tpl"}

    {if isset($confirmation) && $confirmation}
        <p class="alert alert-success">
            {if $lang_iso == 'es'}
                {l s='Tu información personal ha sido actualizada con éxito.'}
            {else}
                {l s='Your personal information has been successfully updated.'}
            {/if}            
            {if isset($pwd_changed)}<br />
                {if $lang_iso == 'es'}
                    {l s='Tu contraseña ha sido enviada a tu correo:'}
                {else}
                    {l s='Your password has been sent to your email:'}
                {/if}                
                &nbsp;{$email}
            {/if}
        </p>
    {else}
        <p class="info-title">
            {if $lang_iso == 'es'}
                {l s='Por favor, asegúrate de actualizar tu información personal si esta cambia.'}
            {else}
                {l s='Please be sure to update your personal information if it has changed.'}
            {/if}
        </p>
        <p class="required">
            <sup>*</sup>
            {if $lang_iso == 'es'}
                {l s='Campos requeridos'}
            {else}
                {l s='Required field'}
            {/if}            
        </p>
        <form action="{$link->getPageLink('identity', true)|escape:'html':'UTF-8'}" method="post" class="std">
            <fieldset>
                <div class="clearfix">
                    <label>
                        {if $lang_iso == 'es'}
                            {l s='Título social'}
                        {else}
                            {l s='Social title'}
                        {/if}                        
                    </label>
                    <br />
                    {foreach from=$genders key=k item=gender}
                        <div class="radio-inline">
                            <label for="id_gender{$gender->id}" class="top">
                            <input type="radio" name="id_gender" id="id_gender{$gender->id}" value="{$gender->id|intval}" {if isset($smarty.post.id_gender) && $smarty.post.id_gender == $gender->id}checked="checked"{/if} />
                            {$gender->name}</label>
                        </div>
                    {/foreach}
                </div>
                <div class="required form-group">
                    <label for="firstname" class="required">
                        {if $lang_iso == 'es'}
                            {l s='Nombres'}
                        {else}
                            {l s='First name'}
                        {/if}                        
                    </label>
                    <input class="is_required validate form-control" data-validate="isName" type="text" id="firstname" name="firstname" value="{$smarty.post.firstname}" />
                </div>
                <div class="required form-group">
                    <label for="lastname" class="required">
                        {if $lang_iso == 'es'}
                            {l s='Apellidos'}
                        {else}
                            {l s='Last name'}
                        {/if}
                    </label>
                    <input class="is_required validate form-control" data-validate="isName" type="text" name="lastname" id="lastname" value="{$smarty.post.lastname}" />
                </div>
                <div class="required form-group">
                    <label for="email" class="required">
                        {if $lang_iso == 'es'}
                            {l s='Dirección de E-mail'}
                        {else}
                            {l s='E-mail address'}
                        {/if}                        
                    </label>
                    <input class="is_required validate form-control" data-validate="isEmail" type="email" name="email" id="email" value="{$smarty.post.email}" />
                </div>
                <div class="form-group">
                    <label>
                        {if $lang_iso == 'es'}
                            {l s='Fecha de nacimiento'}
                        {else}
                            {l s='Date of Birth'}
                        {/if}                        
                    </label>
                    <div class="row">
                        <div class="col-xs-4">
                            <select name="days" id="days" class="form-control">
                                <option value="">-</option>
                                {foreach from=$days item=v}
                                    <option value="{$v}" {if ($sl_day == $v)}selected="selected"{/if}>{$v}&nbsp;&nbsp;</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-xs-4">
							{*
								{l s='January'}
								{l s='February'}
								{l s='March'}
								{l s='April'}
								{l s='May'}
								{l s='June'}
								{l s='July'}
								{l s='August'}
								{l s='September'}
								{l s='October'}
								{l s='November'}
								{l s='December'}
							*}
                            <select id="months" name="months" class="form-control">
                                <option value="">-</option>
                                {foreach from=$months key=k item=v}
                                    <option value="{$k}" {if ($sl_month == $k)}selected="selected"{/if}>
                                        {if $lang_iso == 'es'}
                                            {if $k == 1}
                                                Enero
                                            {else if $k == 2}
                                                Febrero
                                            {else if $k == 3}
                                                Marzo
                                            {else if $k == 4}
                                                Abril
                                            {else if $k == 5}
                                                Mayo
                                            {else if $k == 6}
                                                Junio
                                            {else if $k == 7}
                                                Julio
                                            {else if $k == 8}
                                                Agosto
                                            {else if $k == 9}
                                                Septiembre
                                            {else if $k == 10}
                                                Octubre
                                            {else if $k == 11}
                                                Noviembre
                                            {else if $k == 12}
                                                Diciembre
                                            {else}
                                                {l s=$v}
                                            {/if}
                                        {else}
                                            {l s=$v}
                                        {/if}
                                        &nbsp;
                                    </option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-xs-4">
                            <select id="years" name="years" class="form-control">
                                <option value="">-</option>
                                {foreach from=$years item=v}
                                    <option value="{$v}" {if ($sl_year == $v)}selected="selected"{/if}>{$v}&nbsp;&nbsp;</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="required form-group">
                    <label for="old_passwd" class="required">
                        {if $lang_iso == 'es'}
                            {l s='Contraseña Actual'}
                        {else}
                            {l s='Current Password'}
                        {/if}                        
                    </label>
                    <input class="is_required validate form-control" type="password" data-validate="isPasswd" name="old_passwd" id="old_passwd" />
                </div>
                <div class="password form-group">
                    <label for="passwd">
                        {if $lang_iso == 'es'}
                            {l s='Contraseña Nueva'}
                        {else}
                            {l s='New Password'}
                        {/if}                        
                    </label>
                    <input class="is_required validate form-control" type="password" data-validate="isPasswd" name="passwd" id="passwd" />
                </div>
                <div class="password form-group">
                    <label for="confirmation">
                        {if $lang_iso == 'es'}
                            {l s='Confirmación'}
                        {else}
                            {l s='Confirmation'}
                        {/if}                        
                    </label>
                    <input class="is_required validate form-control" type="password" data-validate="isPasswd" name="confirmation" id="confirmation" />
                </div>
                {if isset($newsletter) && $newsletter}
                    <div class="checkbox">
                        <label for="newsletter">
                            <input type="checkbox" id="newsletter" name="newsletter" value="1" {if isset($smarty.post.newsletter) && $smarty.post.newsletter == 1} checked="checked"{/if}/>
                            {if $lang_iso == 'es'}
                                {l s='¡Suscríbete a nuestro boletín!'}
                            {else}
                                {l s='Sign up for our newsletter!'}
                            {/if}                            
                            {if isset($required_fields) && array_key_exists('newsletter', $field_required)}
                              <sup> *</sup>
                            {/if}
                        </label>
                    </div>
                {/if}
                {if isset($optin) && $optin}
                    <div class="checkbox">
                        <label for="optin">
                            <input type="checkbox" name="optin" id="optin" value="1" {if isset($smarty.post.optin) && $smarty.post.optin == 1} checked="checked"{/if}/>
                            {if $lang_iso == 'es'}
                                {l s='¡Recibe ofertas especiales de nuestros asociados!'}
                            {else}
                                {l s='Receive special offers from our partners!'}
                            {/if}                            
                            {if isset($required_fields) && array_key_exists('optin', $field_required)}
                              <sup> *</sup>
                            {/if}
                        </label>
                    </div>
                {/if}
			{if $b2b_enable}
				<h1 class="page-subheading">
					{l s='Your company information'}
				</h1>
				<div class="form-group">
					<label for="">{l s='Company'}</label>
					<input type="text" class="form-control" id="company" name="company" value="{if isset($smarty.post.company)}{$smarty.post.company}{/if}" />
				</div>
				<div class="form-group">
					<label for="siret">{l s='SIRET'}</label>
					<input type="text" class="form-control" id="siret" name="siret" value="{if isset($smarty.post.siret)}{$smarty.post.siret}{/if}" />
				</div>
				<div class="form-group">
					<label for="ape">{l s='APE'}</label>
					<input type="text" class="form-control" id="ape" name="ape" value="{if isset($smarty.post.ape)}{$smarty.post.ape}{/if}" />
				</div>
				<div class="form-group">
					<label for="website">{l s='Website'}</label>
					<input type="text" class="form-control" id="website" name="website" value="{if isset($smarty.post.website)}{$smarty.post.website}{/if}" />
				</div>
			{/if}
                {if isset($HOOK_CUSTOMER_IDENTITY_FORM)}
			{$HOOK_CUSTOMER_IDENTITY_FORM}
		{/if}
                <div class="form-group">
                    <button type="submit" name="submitIdentity" class="btn btn-default button button-medium">
                        <span>
                            {if $lang_iso == 'es'}
                                {l s='Guardar'}
                            {else}
                                {l s='Save'}
                            {/if}                            
                            <i class="icon-chevron-right right"></i>
                        </span>
                    </button>
                </div>
            </fieldset>
        </form> <!-- .std -->
    {/if}
</div>
<ul class="footer_links clearfix">
	<li>
        <a class="btn btn-default button button-small" href="{$link->getPageLink('my-account', true)}">
            <span>
                <i class="icon-chevron-left"></i>
                {if $lang_iso == 'es'}
                    {l s='Regresar a tu cuenta'}
                {else}
                    {l s='Back to your account'}
                {/if}
            </span>
        </a>
    </li>
	<li>
        <a class="btn btn-default button button-small" href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}">
            <span>
                <i class="icon-chevron-left"></i>
                {if $lang_iso == 'es'}
                    {l s='Inicio'}
                {else}
                    {l s='Home'}
                {/if}                
            </span>
        </a>
    </li>
</ul>
