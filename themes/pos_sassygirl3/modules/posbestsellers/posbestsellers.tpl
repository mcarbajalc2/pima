<!--
<div class="container">
	<div class="pos-bestsellers">
		
		<div class="pos-title">
			<h2>
				<a href="{$link->getPageLink('best-sales')|escape:'html'}" title="{l s='View a top sellers products' mod='posbestsellers'}">
					{l s='Top sellers' mod='posbestsellers'}
				</a>
			</h2>
		</div>
		{if $best_sellers && $best_sellers|@count > 0}
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<div class="row">
					<ul class="owl">
						{$rows=1}
						{$i=0}
						{foreach from=$best_sellers item=product name=myLoop}
							{if $i%$rows ==0} 
							<li class='bestsellerproductslider-item'>
							{/if}
								<div class="item-product">
									<div class="products-inner">
										<a class ="bigpic_{$product.id_product}_tabcategory product_image" href="{$product.link|escape:'html'}" title="{$product.name|escape:html:'UTF-8'}">
											<img class="img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'thickbox_default')|escape:'html'}" alt="{$product.name|escape:html:'UTF-8'}" />								
										</a>
										{if isset($product.new) && $product.new == 1}
											<a class="new-box" href="{$product.link|escape:'html':'UTF-8'}">
												<span class="new-label">{l s='New' mod='posbestsellers'}</span>
											</a>
										{/if}
										{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
											<a class="sale-box" href="{$product.link|escape:'html':'UTF-8'}">
												<span class="sale-label">{l s='Sale!' mod='posbestsellers'}</span>
											</a>
										{/if}
										{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
											{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
												{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
													{if $product.specific_prices.reduction_type == 'percentage'}
														<span class="price-percent-reduction"><span>-{$product.specific_prices.reduction * 100}%</span></span>
													{/if}
												{/if}										
											{/if}						
										{/if}
										<div class="actions">
													
											<div class="actions-inner">
												
												<ul class="add-to-links">
													<li class="cart">
														{if ($product.id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $product.available_for_order && !isset($restricted_country_mode) && $product.minimal_quantity <= 1 && $product.customizable != 2 && !$PS_CATALOG_MODE}
														{if ($product.allow_oosp || $product.quantity > 0)}
														{if isset($static_token)}
															<a class="button ajax_add_to_cart_button btn btn-default" href="{$link->getPageLink('cart',false, NULL, "add=1&amp;id_product={$product.id_product|intval}&amp;token={$static_token}", false)|escape:'html':'UTF-8'}" rel="nofollow"  title="{l s='Add to cart' mod='posbestsellers'}" data-id-product="{$product.id_product|intval}">
																<span>{l s='Add to cart' mod='posbestsellers'}</span>
																
															</a>
														{else}
														<a class="button ajax_add_to_cart_button btn btn-default" href="{$link->getPageLink('cart',false, NULL, 'add=1&amp;id_product={$product.id_product|intval}', false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart' mod='posbestsellers'}" data-id-product="{$product.id_product|intval}">
															<span>{l s='Add to cart' mod='posbestsellers'}</span>
														</a>
														   {/if}      
														{else}
														<span class="button ajax_add_to_cart_button btn btn-default disabled" >
															<span>{l s='Add to cart' mod='posbestsellers'}</span>
														</span>
														{/if}
														{/if}
													</li>
													<li>
														<a class="addToWishlist wishlistProd_{$product.id_product|intval}" href="#" data-wishlist="{$product.id_product|intval}" title="{l s='Add to Wishlist' mod='posbestsellers'}" onclick="WishlistCart('wishlist_block_list', 'add', '{$product.id_product|intval}', false, 1); return false;">
															<span>{l s='Wishlist' mod='posbestsellers'}</span>
															
														</a>
													</li>
													<li>
													{if isset($comparator_max_item) && $comparator_max_item}
													  <a class="add_to_compare" href="{$product.link|escape:'html':'UTF-8'}" data-id-product="{$product.id_product}" title="{l s='Add to Compare' mod='posbestsellers'}">{l s='Compare' mod='posbestsellers'}
													
													  </a>
													 {/if}
													</li>
													<li>
														{if isset($quick_view) && $quick_view}
															<a class="quick-view" title="{l s='Quick view' mod='posbestsellers'}" href="{$product.link|escape:'html':'UTF-8'}">

															</a>
														{/if}
													</li>
												</ul>
											</div>
										</div>	
								
									</div>
									<div class="product-contents">
										<h5 class="product-name"><a href="{$product.link|escape:'html'}" title="{$product.name|truncate:50:'...'|escape:'htmlall':'UTF-8'}">{$product.name|truncate:25:'...'|escape:'htmlall':'UTF-8'}</a></h5>
										{capture name='displayProductListReviews'}{hook h='displayProductListReviews' product=$product}{/capture}
											{if $smarty.capture.displayProductListReviews}
												<div class="hook-reviews">
												{hook h='displayProductListReviews' product=$product}
												</div>
										{/if}
										
							
									</div>
									{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
									<div class="price-box">
									{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
										{hook h="displayProductPriceBlock" product=$product type='before_price'}
										<span class="price product-price">
											{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}
										</span>
										{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
											{hook h="displayProductPriceBlock" product=$product type="old_price"}
											<span class="old-price product-price">
												{displayWtPrice p=$product.price_without_reduction}
											</span>
											{hook h="displayProductPriceBlock" id_product=$product.id_product type="old_price"}
											
										{/if}
										{hook h="displayProductPriceBlock" product=$product type="price"}
										{hook h="displayProductPriceBlock" product=$product type="unit_price"}
										{hook h="displayProductPriceBlock" product=$product type='after_price'}
									{/if}
									</div>
									{/if}
							
								</div>
							{$i= $i +1}
							{if $i %$rows == 0 } 
							</li> 
							{/if}                    
						{/foreach}
					</ul>
				</div>	
			</div>		
			<div class="hidden-xs col-xs-12 col-sm-6">
				<div class="row">
					<div class="best-right small-inner ">
					{$j=0} 
					{foreach from=$best_sellers item=product name=myLoop}
					{if $j >= $row} 
						{if $smarty.foreach.myLoop.index % $row == 0 || $smarty.foreach.myLoop.first }
						<ul>
						{/if} 
							<li class="best{$j}" onclick="bestslider({$j})">
								<div class="item-inner">
									<img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html'}"
										alt="{$product.legend|escape:'html':'UTF-8'}"/>
									<div class="product-contents">
										{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
										<div class="price-box">
										{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
											{hook h="displayProductPriceBlock" product=$product type='before_price'}
											<span class="price product-price">
												{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}
											</span>
											{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
												{hook h="displayProductPriceBlock" product=$product type="old_price"}
												<span class="old-price product-price">
													{displayWtPrice p=$product.price_without_reduction}
												</span>
												{hook h="displayProductPriceBlock" id_product=$product.id_product type="old_price"}
												
											{/if}
											{hook h="displayProductPriceBlock" product=$product type="price"}
											{hook h="displayProductPriceBlock" product=$product type="unit_price"}
											{hook h="displayProductPriceBlock" product=$product type='after_price'}
										{/if}
										</div>
										{/if}
							
									</div>
								</div>
							</li>	
						{if $smarty.foreach.myLoop.iteration % $row == 0 || $smarty.foreach.myLoop.last  }	
						</ul>
						{/if}
					{/if}	
						{$j = $j + 1} 
				   {/foreach}
					</div>
					<div class="boxprevnext">
						<a class="prev prevsellers"><i class="icon-angle-left"></i></a>
						<a class="next nextsellers"><i class="icon-angle-right"></i></a>
					</div>
				</div>
			</div>		
		</div>
		<script type="text/javascript">
			// $jq(document).ready(function(){
				$(".pos-bestsellers .owl").owlCarousel({
					autoPlay : true,
					 items : {$item},
					pagination :false,
					itemsDesktop : [1199,1],
					itemsDesktopSmall : [980,1],
					itemsTablet: [768,1],
					itemsMobile : [479,1],
					stopOnHover : true,
					 pagination :false,
					scrollPerPage:true,
					afterMove: function(){
						x = $( ".pos-bestsellers .owl-pagination .owl-page" ).index( $( ".pos-bestsellers .owl-pagination .active" ));
						var bthumb = ".best"+x;
						$(".best-left li").removeClass('active');
						$(".best-right li").removeClass('active');
						$(bthumb).addClass('active');
					}
				});
				var bslider = $(".pos-bestsellers .owl").data('owlCarousel');
	
				function bestslider(x)
				{
					bslider.goTo(x)
				}
			// });
		</script>
	   {else}

		<div class="ma-bestseller-slider">	
			   <p>{l s='No best sellers at this time' mod='posbestsellers'}</p>
		</div>
				
		{/if}
	</div> 
</div> 	
<script type="text/javascript"> 
	$(document).ready(function() {
		var owl = $(".best-right");
		owl.owlCarousel({
		items :2,
		slideSpeed: 1000,
		 pagination :false,
		itemsDesktop : [1199,2],
		itemsDesktopSmall : [991,2],
		itemsTablet: [767,2],
		itemsMobile : [480,1]
		});
		$(".nextsellers").click(function(){
		owl.trigger('owl.next');
		})
		$(".prevsellers").click(function(){
		owl.trigger('owl.prev');
		})  
	});
</script>
-->