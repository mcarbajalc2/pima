{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div id="mywishlist">
	{capture name=path}
		<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
			{if $lang_iso == 'es'}
				{l s='Mi cuenta' mod='blockwishlist'}
			{else}
				{l s='My account' mod='blockwishlist'}
			{/if}			
		</a>
		<span class="navigation-pipe">
			{$navigationPipe}
		</span>
		<span class="navigation_page">
			{if $lang_iso == 'es'}
				{l s='Mis listas de deseos' mod='blockwishlist'}
			{else}
				{l s='My wishlists' mod='blockwishlist'}
			{/if}			
		</span>
	{/capture}

	<h1 class="page-heading">
		{if $lang_iso == 'es'}
			{l s='Mis listas de deseos' mod='blockwishlist'}
		{else}
			{l s='My wishlists' mod='blockwishlist'}
		{/if}
	</h1>

	{include file="$tpl_dir./errors.tpl"}

	{if $id_customer|intval neq 0}
		<form method="post" class="std box" id="form_wishlist">
			<fieldset>
				<h3 class="page-subheading">
					{if $lang_iso == 'es'}
						{l s='Nueva lista de deseos ' mod='blockwishlist'}
					{else}
						{l s='New wishlist' mod='blockwishlist'}
					{/if}					
				</h3>
				<div class="form-group">
					<input type="hidden" name="token" value="{$token|escape:'html':'UTF-8'}" />
					<label class="align_right" for="name">
						{if $lang_iso == 'es'}
							{l s='Nombre' mod='blockwishlist'}
						{else}
							{l s='Name' mod='blockwishlist'}
						{/if}						
					</label>
					<input type="text" id="name" name="name" class="inputTxt form-control" value="{if isset($smarty.post.name) and $errors|@count > 0}{$smarty.post.name|escape:'html':'UTF-8'}{/if}" />
				</div>
				<p class="submit">
                    <button id="submitWishlist" class="btn btn-default button button-medium" type="submit" name="submitWishlist">
                    	<span>
                    		{if $lang_iso == 'es'}
                    			{l s='Guardar' mod='blockwishlist'}
                    		{else}
                    			{l s='Save' mod='blockwishlist'}
                    		{/if}                    		
                    		<i class="icon-chevron-right right"></i>
                    	</span>
                    </button>
				</p>
			</fieldset>
		</form>
		{if $wishlists}
			<div id="block-history" class="block-center">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th class="first_item">
								{if $lang_iso == 'es'}
									{l s='Nombre' mod='blockwishlist'}
								{else}
									{l s='Name' mod='blockwishlist'}
								{/if}								
							</th>
							<th class="item mywishlist_first">
								{if $lang_iso == 'es'}
									{l s='Cant.' mod='blockwishlist'}
								{else}
									{l s='Qty' mod='blockwishlist'}
								{/if}								
							</th>
							<th class="item mywishlist_first">
								{if $lang_iso == 'es'}
									{l s='Visto' mod='blockwishlist'}
								{else}
									{l s='Viewed' mod='blockwishlist'}
								{/if}								
							</th>
							<th class="item mywishlist_second">
								{if $lang_iso == 'es'}
									{l s='Creado' mod='blockwishlist'}
								{else}
									{l s='Created' mod='blockwishlist'}
								{/if}								
							</th>
							<th class="item mywishlist_second">
								{if $lang_iso == 'es'}
									{l s='Link Directo' mod='blockwishlist'}
								{else}
									{l s='Direct Link' mod='blockwishlist'}
								{/if}								
							</th>
							<th class="item mywishlist_second">
								{if $lang_iso == 'es'}
									{l s='Por Defecto' mod='blockwishlist'}
								{else}
									{l s='Default' mod='blockwishlist'}
								{/if}								
							</th>
							<th class="last_item mywishlist_first">
								{if $lang_iso == 'es'}
									{l s='Eliminar' mod='blockwishlist'}
								{else}
									{l s='Delete' mod='blockwishlist'}
								{/if}								
							</th>
						</tr>
					</thead>
					<tbody>
						{section name=i loop=$wishlists}
							<tr id="wishlist_{$wishlists[i].id_wishlist|intval}">
								<td style="width:200px;">
									<a href="#" onclick="javascript:event.preventDefault();WishlistManage('block-order-detail', '{$wishlists[i].id_wishlist|intval}');">
										{$wishlists[i].name|truncate:30:'...'|escape:'html':'UTF-8'}
									</a>
								</td>
								<td class="bold align_center">
									{assign var=n value=0}
									{foreach from=$nbProducts item=nb name=i}
										{if $nb.id_wishlist eq $wishlists[i].id_wishlist}
											{assign var=n value=$nb.nbProducts|intval}
										{/if}
									{/foreach}
									{if $n}
										{$n|intval}
									{else}
										0
									{/if}
								</td>
								<td>{$wishlists[i].counter|intval}</td>
								<td>{$wishlists[i].date_add|date_format:"%Y-%m-%d"}</td>
								<td>
									<a href="#" onclick="javascript:event.preventDefault();WishlistManage('block-order-detail', '{$wishlists[i].id_wishlist|intval}');">
										{if $lang_iso == 'es'}
											{l s='Ver' mod='blockwishlist'}
										{else}
											{l s='View' mod='blockwishlist'}
										{/if}										
									</a>
								</td>
								<td class="wishlist_default">
									{if isset($wishlists[i].default) && $wishlists[i].default == 1}
										<p class="is_wish_list_default">
											<i class="icon icon-check-square"></i>
										</p>
									{else}
										<a href="#" onclick="javascript:event.preventDefault();(WishlistDefault('wishlist_{$wishlists[i].id_wishlist|intval}', '{$wishlists[i].id_wishlist|intval}'));">
											<i class="icon icon-square"></i>
										</a>
									{/if}
								</td>
								<td class="wishlist_delete">
									<a class="icon" href="#" onclick="javascript:event.preventDefault();return (WishlistDelete('wishlist_{$wishlists[i].id_wishlist|intval}', '{$wishlists[i].id_wishlist|intval}', '{l s='Do you really want to delete this wishlist ?' mod='blockwishlist' js=1}'));">
										<i class="icon-remove"></i>
									</a>
								</td>
							</tr>
						{/section}
					</tbody>
				</table>
			</div>
			<div id="block-order-detail">&nbsp;</div>
		{/if}
	{/if}
	<ul class="footer_links clearfix">
		<li>
			<a class="btn btn-default button button-small" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
				<span>
					<i class="icon-chevron-left"></i>
					{if $lang_iso == 'es'}
						{l s='Regresar a Tu Cuenta' mod='blockwishlist'}
					{else}
						{l s='Back to Your Account' mod='blockwishlist'}
					{/if}					
				</span>
			</a>
		</li>
		<li>
			<a class="btn btn-default button button-small" href="{$base_dir|escape:'html':'UTF-8'}">
				<span>
					<i class="icon-chevron-left"></i>
					{if $lang_iso == 'es'}
						{l s='Inicio' mod='blockwishlist'}
					{else}
						{l s='Home' mod='blockwishlist'}
					{/if}					
				</span>
			</a>
		</li>
	</ul>
</div>
