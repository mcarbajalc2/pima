<script type="text/javascript">

$(document).ready(function() {

	$(".tab_category").hide();
	$(".tab_category:first").show(); 

	$("ul.tab_cates li").click(function() {
		$("ul.tab_cates li").removeClass("active");
		$(this).addClass("active");
		$(".tab_category").hide();
		$(".tab_category").removeClass("animate1 {$tab_effect}");
		var activeTab = $(this).attr("rel"); 
		$("#"+activeTab) .addClass("animate1 {$tab_effect}");
		$("#"+activeTab).fadeIn(); 
	});
});

</script>

<div class="tab-category-container-slider">
	<div class="container-tab">
		<div class="container-inner">
			<div class="tab-category">	
				<div class="pos_tab">
				
					<div class ='pos-title'>
							
						<h2>
							<span>
								{if $lang_iso == 'es'}
									{l s='Todos los Productos' mod='postabcateslider1'}
								{else}
									{l s='all Products' mod='postabcateslider1'}
								{/if}							
							</span>
						</h2>
						
					</div> 
					<ul class="tab_cates"> 
						{$count=0}
						{foreach from=$productCates item=productCate name=posTabCategory}
								<li data-title="tabtitle_{$productCate.id}" rel="tab_{$productCate.id}" {if $count==0} class="active"  {/if} > {$productCate.name}</li>
								{$count= $count+1}
						{/foreach}	
					</ul>

				</div>
				<div class="row  pos-content">
				
					<div class="tab1_container"> 
					{foreach from=$productCates item=productCate name=posTabCategory}
						
						<div id="tab_{$productCate.id}" class="tab_category">
							<div>					

								<div class="productTabCategorySlider{$productCate.id}">
									{foreach from=$productCate.product item=product name=myLoop}
									
										<div class="cate_item col-lg-4 col-md-4 col-sm-4 col-xs-6 wow fadeInUp" data-wow-delay="{$smarty.foreach.myLoop.index}00ms">
								
											<div class="item-product">
												<div class="products-inner">
													<a class ="bigpic_{$product.id_product}_tabcategory product_image" href="{$product.link|escape:'html'}" title="{$product.name|escape:html:'UTF-8'}">
														<img class="img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html'}" alt="{$product.name|escape:html:'UTF-8'}" />								
													</a>
													{if isset($product.new) && $product.new == 1}
														<a class="new-box" href="{$product.link|escape:'html':'UTF-8'}">
															<span class="new-label">{l s='New' mod='postabcateslider1'}</span>
														</a>
													{/if}
													{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
														<a class="sale-box" href="{$product.link|escape:'html':'UTF-8'}">
															<span class="sale-label">{l s='Sale!' mod='postabcateslider1'}</span>
														</a>
													{/if}
													{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
														{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
															{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
																{if $product.specific_prices.reduction_type == 'percentage'}
																	<span class="price-percent-reduction"><span>-{$product.specific_prices.reduction * 100}%</span></span>
																{/if}
															{/if}										
														{/if}						
													{/if}
													<div class="actions">
																
														<div class="actions-inner">
															
															<ul class="add-to-links">
																<li class="cart">
																	{if ($product.id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $product.available_for_order && !isset($restricted_country_mode) && $product.minimal_quantity <= 1 && $product.customizable != 2 && !$PS_CATALOG_MODE}
																	{if ($product.allow_oosp || $product.quantity > 0)}
																	{if isset($static_token)}
																		<a class="button ajax_add_to_cart_button btn btn-default" href="{$link->getPageLink('cart',false, NULL, "add=1&amp;id_product={$product.id_product|intval}&amp;token={$static_token}", false)|escape:'html':'UTF-8'}" rel="nofollow"  title="{l s='Add to cart' mod='postabcateslider1'}" data-id-product="{$product.id_product|intval}">
																			<span>{l s='Add to cart' mod='postabcateslider1'}</span>
																			
																		</a>
																	{else}
																	<a class="button ajax_add_to_cart_button btn btn-default" href="{$link->getPageLink('cart',false, NULL, 'add=1&amp;id_product={$product.id_product|intval}', false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart' mod='postabcateslider1'}" data-id-product="{$product.id_product|intval}">
																		<span>{l s='Add to cart' mod='postabcateslider1'}</span>
																	</a>
																	   {/if}      
																	{else}
																	<span class="button ajax_add_to_cart_button btn btn-default disabled" >
																		<span>{l s='Add to cart' mod='postabcateslider1'}</span>
																	</span>
																	{/if}
																	{/if}
																</li>
																<li>
																	<a class="addToWishlist wishlistProd_{$product.id_product|intval}" href="#" data-wishlist="{$product.id_product|intval}" title="{l s='Add to Wishlist' mod='postabcateslider1'}" onclick="WishlistCart('wishlist_block_list', 'add', '{$product.id_product|intval}', false, 1); return false;">
																		<span>{l s='Wishlist' mod='postabcateslider1'}</span>
																		
																	</a>
																</li>
																<li>
																{if isset($comparator_max_item) && $comparator_max_item}
																  <a class="add_to_compare" href="{$product.link|escape:'html':'UTF-8'}" data-id-product="{$product.id_product}" title="{l s='Add to Compare' mod='postabcateslider1'}">{l s='Compare' mod='postabcateslider1'}
																
																  </a>
																 {/if}
																</li>
																<li>
																	{if isset($quick_view) && $quick_view}
																		<a class="quick-view" title="{l s='Quick view' mod='postabcateslider1'}" href="{$product.link|escape:'html':'UTF-8'}">

																		</a>
																	{/if}
																</li>
															</ul>
														</div>
													</div>	
											
												</div>
												<div class="product-contents">

													<h5 class="product-name"><a href="{$product.link|escape:'html'}" title="{$product.name|truncate:50:'...'|escape:'htmlall':'UTF-8'}"><!--{$product.name|truncate:25:'...'|escape:'htmlall':'UTF-8'}-->{$product.name|escape:'htmlall':'UTF-8'}</a></h5>
													{capture name='displayProductListReviews'}{hook h='displayProductListReviews' product=$product}{/capture}
														{if $smarty.capture.displayProductListReviews}
															<div class="hook-reviews">
															{hook h='displayProductListReviews' product=$product}
															</div>
													{/if}
													{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
													<div class="price-box">
													{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
														{hook h="displayProductPriceBlock" product=$product type='before_price'}
														<span class="price product-price">
															{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}
														</span>
														{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
															{hook h="displayProductPriceBlock" product=$product type="old_price"}
															<span class="old-price product-price">
																{displayWtPrice p=$product.price_without_reduction}
															</span>
															{hook h="displayProductPriceBlock" id_product=$product.id_product type="old_price"}
															
														{/if}
														{hook h="displayProductPriceBlock" product=$product type="price"}
														{hook h="displayProductPriceBlock" product=$product type="unit_price"}
														{hook h="displayProductPriceBlock" product=$product type='after_price'}
													{/if}
													</div>
													{/if}
										
												</div>
										
											</div>
												
										</div>
										
									{/foreach}
									
								</div>
								{if count($productCate.product)>4}
								<div id="loadMore{$productCate.id}" class="loadmore">
									<span>
										{if $lang_iso == es}
											{l s='Mostrar mas' mod='postabcateslider1'}
										{else}
											{l s='Load more' mod='postabcateslider1'}
										{/if}
									
									</span>
								</div>
								<!-- <div id="showLess{$productCate.id}" class="showless"><span>{l s='Show less' mod='postabcateslider1'}</span></div> -->
								 {/if}
								<script type="text/javascript">

								$(document).ready(function () {
									//size_div = $(".productTabCategorySlider{$productCate.id} .cate_item")
									$(".productTabCategorySlider{$productCate.id} .cate_item").hide();
									size_div = $(".productTabCategorySlider{$productCate.id} .cate_item").size();
									// number products show in the first
									var x={$slideOptions.number_product};									
									// number products want to show 	
									var n = 4; 
									var np = {count($productCate.product)};
									//console.log(np);
									$(".productTabCategorySlider{$productCate.id} .cate_item:lt("+x+")") .show();
									$('#showLess{$productCate.id}').hide();	
									if(x >= np){
										$('#loadMore{$productCate.id}').hide();
									}
									$("#loadMore{$productCate.id}").click(function () {
									
										x= (x+n <= size_div) ? x+n : size_div;
										
										$(".productTabCategorySlider{$productCate.id} .cate_item:lt("+x+")") .addClass(" animated") .fadeIn();
										$(".productTabCategorySlider{$productCate.id} .cate_item:lt("+x+")") .attr("style","visibility: visible; animation-name: fadeInUp;");
										//console.log(".productTabCategorySlider{$productCate.id} .cate_item:lt("+x+")");
										$('#showLess{$productCate.id}').show();
										if(x >= np){
											$('#loadMore{$productCate.id}').hide();
										}
										
									});
									
									 $("#showLess{$productCate.id}").click(function () {
											x=(x-n<0) ? n : x-n;
											$(".productTabCategorySlider{$productCate.id} .cate_item").not(':lt('+x+')') .fadeOut();
												$(".productTabCategorySlider{$productCate.id} .cate_item:lt("+x+")") .attr("style","visibility: visible; animation-name: fadeInUp;");
											$('#loadMore{$productCate.id}').show();
											if(x <= np){
												$('#showLess{$productCate.id}').hide();	
											}
									  });
									  
									
								});

								</script>
							
							</div>
						
						</div>
								
					{/foreach}	
					 </div> <!-- .tab_container -->
				
				</div>
			</div>
		</div>
	</div>
</div>

