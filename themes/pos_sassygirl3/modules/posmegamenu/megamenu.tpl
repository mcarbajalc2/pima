
<!-- Block categories module -->
{if $blockCategTree != ''}
	<div class="ma-nav-mobile-container  visible-xs visible-sm ">
		<div class="navbar">
			<div id="navbar-inner" class="navbar-inner navbar-inactive">
				<a class="btn btn-navbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<span class="brand">
					{if $lang_iso == 'es'}
						{l s='Categoría' mod='posmegamenu'}
					{else}
						{l s='Category' mod='posmegamenu'}
					{/if}
				</span>
				<ul id="ma-mobilemenu" class="tree {if $isDhtml}dhtml{/if}  mobilemenu nav-collapse collapse">
					{foreach from=$blockCategTree.children item=child name=blockCategTree}
						{if $smarty.foreach.blockCategTree.last}
							{include file="$branche_tpl_path" node=$child last='true'}
						{else}
							{include file="$branche_tpl_path" node=$child}
						{/if}
					{/foreach}
				</ul>
                                <script type="text/javascript">
                                // <![CDATA[
                                        // we hide the tree only if JavaScript is activated
                                        $('div#categories_block_left ul.dhtml').hide();
                                // ]]>
                                </script>
			</div>
		</div>
</div>
{/if}
<!-- /Block categories module -->

<div class="nav-container visible-lg visible-md ">
    <div id="pt_custommenu" class="pt_custommenu">
        {$megamenu}
        <!-- /Block Search -->
        {if $lang_iso == 'es'}
			<form id="searchbox" method="get" action="//perupima.esfera.pe/es/buscar">
        {else}
			<form id="searchbox" method="get" action="//perupima.esfera.pe/en/search">
        {/if} 
            <input type="hidden" name="controller" value="search">
            <input type="hidden" name="orderby" value="position">
            <input type="hidden" name="orderway" value="desc">
            <input class="search_query form-control ac_input" type="text" id="search_query_top" name="search_query" placeholder="" value="" autocomplete="off">
            <button type="submit" name="submit_search" class="btn btn-default button-search">
            	<!--<img src="/img/cms/if_icon-111-search_314478.svg">-->
				<?xml version="1.0" ?><svg height="40px" version="1.1" viewBox="0 0 32 32" width="40px" xmlns="http://www.w3.org/2000/svg" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns" xmlns:xlink="http://www.w3.org/1999/xlink"><title/><desc/><defs/><g fill="none" fill-rule="evenodd" id="Page-1" stroke="none" stroke-width="1"><g fill="#929292" id="icon-111-search"><path d="M19.4271164,21.4271164 C18.0372495,22.4174803 16.3366522,23 14.5,23 C9.80557939,23 6,19.1944206 6,14.5 C6,9.80557939 9.80557939,6 14.5,6 C19.1944206,6 23,9.80557939 23,14.5 C23,16.3366522 22.4174803,18.0372495 21.4271164,19.4271164 L27.0119176,25.0119176 C27.5621186,25.5621186 27.5575313,26.4424687 27.0117185,26.9882815 L26.9882815,27.0117185 C26.4438648,27.5561352 25.5576204,27.5576204 25.0119176,27.0119176 L19.4271164,21.4271164 L19.4271164,21.4271164 Z M14.5,21 C18.0898511,21 21,18.0898511 21,14.5 C21,10.9101489 18.0898511,8 14.5,8 C10.9101489,8 8,10.9101489 8,14.5 C8,18.0898511 10.9101489,21 14.5,21 L14.5,21 Z" id="search"/></g></g></svg>
            </button>
        </form>
    </div>
</div>

<script type="text/javascript">
//<![CDATA[
var CUSTOMMENU_POPUP_EFFECT = {$effect};
var CUSTOMMENU_POPUP_TOP_OFFSET = {$top_offset};
//]]>
</script>
<script type="text/javascript">
 $(document).ready(function(){
 
  $(window).scroll(function() {    
   var scroll = $(window).scrollTop();
   if (scroll < 245) {
    $(".nav-container").removeClass("scroll-menu");
	    $(".nav-container").children().removeClass("container");
   }else{
    $(".nav-container").addClass("scroll-menu");
	    $(".nav-container").children().addClass("container");
   }
  });
  
  $(".toggle-menu").on("click", function(){
		$(this).toggleClass('show');
		if($(this).hasClass('show')){
			$(this).addClass('open');
			$(this).parent().find('.megamenu').stop().slideDown('slow');
			
		}
		else
		{
			$(this).removeClass('open').addClass('close');
			$(this).parent().find('.megamenu').stop().slideUp('slow');
			
		}
		
	});

 });
</script>
